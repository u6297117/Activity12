import java.util.Scanner;

/**
 * Created by andartafkh on 5/25/17.
 */
public class Counter {
    static int counter;

    public static void main(String[]ar){
        counter=0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Count = "+counter);
        String s;
        do {
            s = scanner.next();
            if(s.equals("i")){
                increament();
            }else if(s.equals("r")){
                counter = reset();
            }else{}
            System.out.println("Count = "+counter);
        }while (!s.equals("q"));
    }
    public static int increament(){
        return ++counter;
    }
    public static int reset(){
        return 0;
    }
}
