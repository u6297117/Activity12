import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by andartafkh on 5/25/17.
 */
public class Testing {
    @Test
    public void test(){
        Counter c = new Counter();
        assertEquals(1,c.increament());
        assertEquals(2,c.increament());
        assertEquals(0,c.reset());
    }
}
